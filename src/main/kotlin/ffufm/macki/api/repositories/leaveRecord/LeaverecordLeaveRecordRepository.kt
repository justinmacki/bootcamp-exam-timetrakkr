package ffufm.macki.api.repositories.leaveRecord

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.macki.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.sql.Date
import java.time.LocalDate

@Repository
interface LeaverecordLeaveRecordRepository : PassRepository<LeaverecordLeaveRecord, Long> {
    @Query(
        """
            SELECT CASE WHEN COUNT(lr) >= 5 THEN TRUE ELSE FALSE END
            FROM LeaverecordLeaveRecord lr WHERE lr.employee.id = :employeeId AND YEAR(lr.date) = YEAR(:date)
            AND MONTH(lr.date) = MONTH(:date) AND lr.leaveType = :leaveType
        """
    )
    fun checkIfAvailableVL(employeeId: Long, date: LocalDate?, leaveType: String): Boolean

    @Query(
        """
            SELECT CASE WHEN COUNT(lr) >= 2 THEN TRUE ELSE FALSE END
            FROM LeaverecordLeaveRecord lr WHERE lr.employee.id = :employeeId AND YEAR(lr.date) = YEAR(:date)
            AND MONTH(lr.date) = MONTH(:date) AND lr.leaveType = :leaveType
        """
    )
    fun checkIfAvailableSL(employeeId: Long, date: LocalDate?, leaveType: String): Boolean

    @Query(
        """
            SELECT COUNT(lr) FROM LeaverecordLeaveRecord lr 
            WHERE lr.employee.id = :employeeId AND YEAR(lr.date) = YEAR(:date)
            AND lr.leaveType = :leaveType
            AND lr.isApproved = true
        """
    )
    fun getRemainingLeave(employeeId: Long, date: LocalDate?, leaveType: String): Int

    @Query(
        """
            SELECT COUNT(lr) FROM LeaverecordLeaveRecord lr 
            WHERE lr.employee.id = :employeeId AND lr.isApproved = false
            AND lr.leaveType = :leaveType
        """
    )
    fun getLeaveRequests(employeeId: Long, leaveType: String): Int

    @Query(
        """
            SELECT COUNT(lr) FROM LeaverecordLeaveRecord lr 
            WHERE lr.employee.id = :employeeId AND YEAR(lr.date) = YEAR(:date)
            AND lr.leaveType = :leaveType AND lr.isApproved = true
        """
    )
    fun getTotalLeavesInCurrentYear(employeeId: Long, date: LocalDate?, leaveType: String): Int

    @Query(
        """
            SELECT COUNT(lr) FROM LeaverecordLeaveRecord lr 
            WHERE lr.employee.id = :employeeId AND YEAR(lr.date) = YEAR(:date)
            AND lr.leaveType = :leaveType AND lr.isApproved = true
        """
    )
    fun getTotalSickDaysInCurrentYear(employeeId: Long, date: LocalDate?, leaveType: String): Int


}