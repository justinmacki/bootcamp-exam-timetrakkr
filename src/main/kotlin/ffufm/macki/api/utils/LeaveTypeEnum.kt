package ffufm.macki.api.utils

enum class LeaveTypeEnum(val value: String) {
    VL("VL"),
    SL("SL")
}