package ffufm.macki.api.repositories.employee

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.macki.api.spec.dbo.employee.EmployeeEmployee
import org.springframework.data.jpa.repository.Query
import kotlin.Long
import org.springframework.stereotype.Repository

@Repository
interface EmployeeEmployeeRepository : PassRepository<EmployeeEmployee, Long>{
    @Query(
        """
            SELECT CASE WHEN COUNT(e) > 0 THEN TRUE ELSE FALSE END
            FROM EmployeeEmployee e WHERE e.email = :email
        """
    )
    fun doesEmailExist(email: String): Boolean
}
