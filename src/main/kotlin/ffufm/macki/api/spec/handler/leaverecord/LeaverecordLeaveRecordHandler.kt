package ffufm.macki.api.spec.handler.leaverecord

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.macki.api.spec.dbo.leaverecord.LeaverecordLeaveRecordDTO
import kotlin.Int
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface LeaverecordLeaveRecordDatabaseHandler {
    /**
     * Approve a Leave: 
     * HTTP Code 200: 
     */
    suspend fun approveLeave(employeeId: Long, id: Long): LeaverecordLeaveRecordDTO

    /**
     * Create LeaveRecord: Creates a new LeaveRecord object
     * HTTP Code 201: The created LeaveRecord
     */
    suspend fun create(body: LeaverecordLeaveRecordDTO, id: Long): LeaverecordLeaveRecordDTO

    /**
     * Get all LeaveRecords per Employee by page: Returns all LeaveRecords from the system that the
     * user has access to. The Headers will include TotalElements, TotalPages, CurrentPage and PerPage
     * to help with Pagination.
     * HTTP Code 200: List of LeaveRecords
     */
    suspend fun getAll(
        id: Long,
        maxResults: Int = 100,
        page: Int = 0
    ): Page<LeaverecordLeaveRecordDTO>

    /**
     * Finds LeaveRecords by ID: Returns LeaveRecords based on ID
     * HTTP Code 200: The LeaveRecord object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): LeaverecordLeaveRecordDTO?

    /**
     * Get No. of Remaining Vacation Leave (Resturlaub EPOS): 
     * HTTP Code 200: Returns the integer value of the total remaining vacation leave of the
     * employee.
     */
    suspend fun getNoOfRemainingVL(id: Long): Int

    /**
     * Get total Annual Leave (Jahresurlaub): 
     * HTTP Code 200: Returns total annual leave
     */
    suspend fun getTotalAnnualLeaves(id: Long): Int

    /**
     * Get No. of Leave Request (Beantragt): 
     * HTTP Code 200: Returns total number of pending leave requests
     */
    suspend fun getTotalLeaveRequests(id: Long): Int

    /**
     * Get No. of Sick Days: 
     * HTTP Code 200: Returns total number of sick days.
     */
    suspend fun getTotalSickDays(id: Long): Int

    /**
     * Delete LeaveRecord by id.: Deletes one specific LeaveRecord.
     */
    suspend fun remove(id: Long)

    /**
     * Update the LeaveRecord: Updates an existing LeaveRecord
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: LeaverecordLeaveRecordDTO, id: Long): LeaverecordLeaveRecordDTO
}

@Controller("leaverecord.LeaveRecord")
class LeaverecordLeaveRecordHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: LeaverecordLeaveRecordDatabaseHandler

    /**
     * Approve a Leave: 
     * HTTP Code 200: 
     */
    @RequestMapping(value = ["/employees/{employeeId:\\d+}/leave-records/{id:\\d+}"], method =
            [RequestMethod.PUT])
    suspend fun approveLeave(@PathVariable("employeeId") employeeId: Long, @PathVariable("id")
            id: Long): ResponseEntity<*> {

        return success { databaseHandler.approveLeave(employeeId, id) }
    }

    /**
     * Create LeaveRecord: Creates a new LeaveRecord object
     * HTTP Code 201: The created LeaveRecord
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/leaverecords/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: LeaverecordLeaveRecordDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body, id) }
    }

    /**
     * Get all LeaveRecords per Employee by page: Returns all LeaveRecords from the system that the
     * user has access to. The Headers will include TotalElements, TotalPages, CurrentPage and PerPage
     * to help with Pagination.
     * HTTP Code 200: List of LeaveRecords
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/leaverecords/"], method = [RequestMethod.GET])
    suspend fun getAll(
        @PathVariable("id") id: Long,
        @RequestParam("maxResults") maxResults: Int? = 100,
        @RequestParam("page") page: Int? = 0
    ): ResponseEntity<*> {

        return paging { databaseHandler.getAll(id, maxResults ?: 100, page ?: 0) }
    }

    /**
     * Finds LeaveRecords by ID: Returns LeaveRecords based on ID
     * HTTP Code 200: The LeaveRecord object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/leaverecords/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Get No. of Remaining Vacation Leave (Resturlaub EPOS): 
     * HTTP Code 200: Returns the integer value of the total remaining vacation leave of the
     * employee.
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/leave-records/no-of-remaining-leave/"], method =
            [RequestMethod.GET])
    suspend fun getNoOfRemainingVL(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getNoOfRemainingVL(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Get total Annual Leave (Jahresurlaub): 
     * HTTP Code 200: Returns total annual leave
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/leave-record/get-total-annual-leaves"], method =
            [RequestMethod.GET])
    suspend fun getTotalAnnualLeaves(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getTotalAnnualLeaves(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Get No. of Leave Request (Beantragt): 
     * HTTP Code 200: Returns total number of pending leave requests
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/leave-records/get-total-leave-requests"], method
            = [RequestMethod.GET])
    suspend fun getTotalLeaveRequests(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getTotalLeaveRequests(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Get No. of Sick Days: 
     * HTTP Code 200: Returns total number of sick days.
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/leave-records/get-total-sick-days/"], method =
            [RequestMethod.GET])
    suspend fun getTotalSickDays(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getTotalSickDays(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Delete LeaveRecord by id.: Deletes one specific LeaveRecord.
     */
    @RequestMapping(value = ["/leaverecords/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the LeaveRecord: Updates an existing LeaveRecord
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/leaverecords/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: LeaverecordLeaveRecordDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
