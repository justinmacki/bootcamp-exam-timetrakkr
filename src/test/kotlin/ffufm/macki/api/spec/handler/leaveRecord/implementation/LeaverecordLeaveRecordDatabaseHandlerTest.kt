package ffufm.macki.api.spec.handler.leaveRecord.implementation

import ffufm.macki.api.PassTestBase
import ffufm.macki.api.spec.handler.leaverecord.LeaverecordLeaveRecordDatabaseHandler
import ffufm.macki.api.spec.handler.utils.EntityGenerator
import ffufm.macki.api.utils.Constants
import ffufm.macki.api.utils.LeaveTypeEnum
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.web.server.ResponseStatusException
import java.sql.Date
import java.time.LocalDate
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith


class LeaverecordLeaveRecordDatabaseHandlerTest : PassTestBase() {

    @Test
    fun `create should return leave record`() = runBlocking {
        assertEquals(0, leaverecordLeaveRecordRepository.findAll().count())

        val employee = EntityGenerator.createEmployee().toDto()
        val createdEmployee = employeeEmployeeDatabaseHandler.create(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().toDto()
        val createdLeaveRecord = leaverecordLeaveRecordDatabaseHandler.create(leaveRecord, createdEmployee.id!!.toLong())

        assertEquals(leaveRecord.leaveType, createdLeaveRecord.leaveType)
        assertEquals(leaveRecord.date, createdLeaveRecord.date)
        assertEquals(leaveRecord.remarks, createdLeaveRecord.remarks)
    }

    @Test
    fun `create leaveRecord should fail given invalid employee id`() = runBlocking {
        val invalidId: Long = Constants.INVALID_ID

        val leaveRecord = EntityGenerator.createLeaveRecord().toDto()

        val exception = assertFailsWith<ResponseStatusException> {
            leaverecordLeaveRecordDatabaseHandler.create(leaveRecord, invalidId)
        }

        val expectedException = "404 NOT_FOUND \"Employee with id: $invalidId not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create leaveRecord should fail given invalid leave type`() = runBlocking {
        val createdEmployee = employeeEmployeeDatabaseHandler.create(EntityGenerator.createEmployee().toDto())

        val leaveRecord = EntityGenerator.createLeaveRecord().toDto().copy(leaveType = "SAMPLE")

        val exception = assertFailsWith<ResponseStatusException> {
            leaverecordLeaveRecordDatabaseHandler.create(leaveRecord, createdEmployee.id!!)
        }

        val expectedException = "409 CONFLICT \"Invalid Leave Type\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create leaveRecord should fail given date is set below 14 days from current date`() = runBlocking {
        val createdEmployee = employeeEmployeeDatabaseHandler.create(EntityGenerator.createEmployee().toDto())

        val leaveRecord = EntityGenerator.createLeaveRecord().toDto().copy(date = LocalDate.parse("2022-01-25"))

        val exception = assertFailsWith<ResponseStatusException> {
            leaverecordLeaveRecordDatabaseHandler.create(leaveRecord, createdEmployee.id!!)
        }

        val expectedException = "400 BAD_REQUEST \"You can only file for a leave 14 days in advance.\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create leaveRecord should fail given 6th vacation leave on the same month and year`() = runBlocking {
        val createdEmployee = employeeEmployeeDatabaseHandler.create(EntityGenerator.createEmployee().toDto())

        val body = EntityGenerator.createLeaveRecord().copy(
            employee = createdEmployee.toEntity(),
            date = LocalDate.parse("2022-02-23")
        )

        leaverecordLeaveRecordRepository.saveAll(
            listOf(
                body,
                body.copy( date = LocalDate.parse("2022-02-28") ),
                body.copy( date = LocalDate.parse("2022-02-27") ),
                body.copy( date = LocalDate.parse("2022-02-26") ),
                body.copy( date = LocalDate.parse("2022-02-25") )
            )
        )

        val sixthVacationLeave = body.copy( date = LocalDate.parse("2022-02-24") )

        val exception = assertFailsWith<ResponseStatusException> {
            leaverecordLeaveRecordDatabaseHandler.create(sixthVacationLeave.toDto(), createdEmployee.id!!)
        }

        val expectedException = "400 BAD_REQUEST \"Employee does not have enough Vacation Leave this month\""

        assertEquals(expectedException, exception.message)    }

    @Test
    fun `getNoOfRemainingVL should return remaining vacation leave for the current year`() = runBlocking {
        val createdEmployee = employeeEmployeeDatabaseHandler.create(EntityGenerator.createEmployee().toDto())

        val body = EntityGenerator.createLeaveRecord().copy(
            employee = createdEmployee.toEntity(),
            isApproved = true
        )

        leaverecordLeaveRecordRepository.saveAll(
            listOf(
                body,
                body.copy( date = LocalDate.parse("2022-01-28") ),
                body.copy( date = LocalDate.parse("2022-01-27") ),
                body.copy( date = LocalDate.parse("2022-01-26") ),
                body.copy( date = LocalDate.parse("2022-01-25") )
            )
        )

        val count = leaverecordLeaveRecordDatabaseHandler.getNoOfRemainingVL(createdEmployee.id!!)

        assertEquals(56, count)
    }

    @Test
    fun `getTotalAnnualLeaves should return number of approved LeaveRecord for the current year`() = runBlocking {
        val createdEmployee = employeeEmployeeDatabaseHandler.create(EntityGenerator.createEmployee().toDto())

        val body = EntityGenerator.createLeaveRecord().copy(
            employee = createdEmployee.toEntity(),
            isApproved = true
        )

        leaverecordLeaveRecordRepository.saveAll(
            listOf(
                body,
                body.copy( date = LocalDate.parse("2022-01-28") ),
                body.copy( date = LocalDate.parse("2022-02-27") ),
                body.copy( date = LocalDate.parse("2022-03-26"), isApproved = false ),
                body.copy( date = LocalDate.parse("2022-04-25") )
            )
        )

        val count = leaverecordLeaveRecordDatabaseHandler.getTotalAnnualLeaves(createdEmployee.id!!)

        assertEquals(4, count)
    }

    @Test
    fun `getTotalLeaveRequests should return number of pending LeaveRecord for the current year`() = runBlocking {
        val createdEmployee = employeeEmployeeDatabaseHandler.create(EntityGenerator.createEmployee().toDto())

        val body = EntityGenerator.createLeaveRecord().copy(
            employee = createdEmployee.toEntity(),
            isApproved = true
        )

        leaverecordLeaveRecordRepository.saveAll(
            listOf(
                body,
                body.copy( date = LocalDate.parse("2022-01-28"), isApproved = false  ),
                body.copy( date = LocalDate.parse("2022-02-27") ),
                body.copy( date = LocalDate.parse("2022-03-26"), isApproved = false ),
                body.copy( date = LocalDate.parse("2022-04-25") )
            )
        )

        val count = leaverecordLeaveRecordDatabaseHandler.getTotalLeaveRequests(createdEmployee.id!!)

        assertEquals(2, count)
    }

    @Test
    fun `getTotalSickDays should return number of pending LeaveRecord for the current year`() = runBlocking {
        val createdEmployee = employeeEmployeeDatabaseHandler.create(EntityGenerator.createEmployee().toDto())

        val body = EntityGenerator.createLeaveRecord().copy(
            employee = createdEmployee.toEntity(),
            isApproved = true,
            leaveType = LeaveTypeEnum.SL.value
        )

        leaverecordLeaveRecordRepository.saveAll(
            listOf(
                body,
                body.copy( date = LocalDate.parse("2022-01-28"), isApproved = false  ),
                body.copy( date = LocalDate.parse("2022-02-27") ),
                body.copy( date = LocalDate.parse("2022-03-26"), isApproved = false ),
                body.copy( date = LocalDate.parse("2022-04-25") )
            )
        )

        val count = leaverecordLeaveRecordDatabaseHandler.getTotalSickDays(createdEmployee.id!!)

        assertEquals(3, count)
    }

}