package ffufm.macki.api.handlerimpl.employee

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.*
import ffufm.macki.api.repositories.employee.EmployeeAddressRepository
import ffufm.macki.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.macki.api.spec.dbo.employee.EmployeeAddress
import ffufm.macki.api.spec.dbo.employee.EmployeeAddressDTO
import ffufm.macki.api.spec.handler.employee.EmployeeAddressDatabaseHandler
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("employee.EmployeeAddressHandler")
class EmployeeAddressHandlerImpl (
    private val employeeRepository : EmployeeEmployeeRepository
        ) : PassDatabaseHandler<EmployeeAddress,
        EmployeeAddressRepository>(), EmployeeAddressDatabaseHandler {

    override suspend fun create(body: EmployeeAddressDTO, id: Long): EmployeeAddressDTO {
        val bodyEntity = body.toEntity()
        val employee = employeeRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id not found")
        }

        if(repository.doesAddressExist(bodyEntity.street, bodyEntity.barangay,
                bodyEntity.city, bodyEntity.province, bodyEntity.zipCode)){
            throw ResponseStatusException(
                HttpStatus.CONFLICT,
                "Address ${body.street}, ${body.barangay}, ${body.city}, ${body.province}, ${body.zipCode} " +
                        "already exists")
        }

        return repository.save(bodyEntity.copy( employee = employee )).toDto()
    }

    override suspend fun getByEmployee(id: Long, maxResults: Int, page: Int): Page<EmployeeAddressDTO> {
        val pagination = PageRequest.of(page ?: 0, maxResults ?: 100)
        return repository.findAll(pagination).toDtos()
    }

    override suspend fun getById(id: Long): EmployeeAddressDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.delete(original)
    }

    override suspend fun update(body: EmployeeAddressDTO, id: Long): EmployeeAddressDTO {
        val bodyEntity = body.toEntity()
        val original = repository.findById(id).orElseThrow404(id)

        return repository.save(original.copy(
            street = bodyEntity.street,
            barangay = bodyEntity.barangay,
            city = bodyEntity.city,
            province = bodyEntity.province,
            zipCode = bodyEntity.zipCode
        )).toDto()
    }
}