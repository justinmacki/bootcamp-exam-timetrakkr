package ffufm.macki.api.handlerimpl.project

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import ffufm.macki.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.macki.api.repositories.project.ProjectProjectRepository
import ffufm.macki.api.spec.dbo.project.ProjectProject
import ffufm.macki.api.spec.dbo.project.ProjectProjectDTO
import ffufm.macki.api.spec.handler.project.ProjectProjectDatabaseHandler
import org.springframework.stereotype.Component

@Component("project.ProjectProjectHandler")
class ProjectProjectHandlerImpl (
    private val employeeRepository : EmployeeEmployeeRepository
) : PassDatabaseHandler<ProjectProject, ProjectProjectRepository>(),
    ProjectProjectDatabaseHandler {

    override suspend fun create(body: ProjectProjectDTO): ProjectProjectDTO {
        TODO("Not yet implemented")
    }

    override suspend fun remove(id: Long) {
        TODO("Not yet implemented")
    }

    override suspend fun update(body: ProjectProjectDTO, id: Long): ProjectProjectDTO {
        TODO("Not yet implemented")
    }
}