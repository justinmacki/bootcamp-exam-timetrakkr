package ffufm.macki.api.repositories.employee

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.macki.api.spec.dbo.employee.EmployeeAddress
import org.springframework.data.jpa.repository.Query

interface EmployeeAddressRepository : PassRepository<EmployeeAddress, Long> {
    @Query(
        """
                SELECT CASE WHEN COUNT(a) > 0 THEN TRUE ELSE FALSE END
                FROM EmployeeAddress a WHERE a.street = :street AND a.barangay = :barangay AND 
                a.city = :city AND a.province = :province AND a.zipCode = :zipCode
        """
    )
    fun doesAddressExist(street: String, barangay: String, city: String, province: String, zipCode: String) : Boolean
}