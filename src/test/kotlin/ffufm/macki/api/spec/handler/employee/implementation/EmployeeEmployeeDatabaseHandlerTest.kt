package ffufm.macki.api.spec.handler.employee.implementation

import ffufm.macki.api.PassTestBase
import ffufm.macki.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.macki.api.spec.dbo.employee.EmployeeEmployee
import ffufm.macki.api.spec.handler.employee.EmployeeEmployeeDatabaseHandler
import ffufm.macki.api.spec.handler.utils.EntityGenerator
import ffufm.macki.api.utils.Constants
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class EmployeeEmployeeDatabaseHandlerTest : PassTestBase() {

    @Test
    fun `create should return user`() = runBlocking {
        val employee = EntityGenerator.createEmployee()
        val superior = employeeEmployeeRepository.save(EntityGenerator.createEmployee())

        val savedUser = employeeEmployeeDatabaseHandler.create(employee.copy(
            superiorId = superior,
            email = "newEmp@email.com"
        ).toDto())

        employeeEmployeeDatabaseHandler.getById(savedUser.id!!)

        assertEquals(2, employeeEmployeeRepository.findAll().size)
    }

    @Test
    fun `create should return error given email already exists`() = runBlocking {
        val employee = EntityGenerator.createEmployee()
        val superior = employeeEmployeeRepository.save(EntityGenerator.createEmployee())

        val exception = assertFailsWith<ResponseStatusException> {
            employeeEmployeeDatabaseHandler.create(employee.copy(
                superiorId = superior
            ).toDto())        }

        val expectedException = "409 CONFLICT \"Email ${employee.email} already exists\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create should return error given invalid employee position`() = runBlocking {
        val employee = EntityGenerator.createEmployee()
        val superior = employeeEmployeeRepository.save(EntityGenerator.createEmployee())

        val exception = assertFailsWith<ResponseStatusException> {
            employeeEmployeeDatabaseHandler.create(employee.copy(
                superiorId = superior,
                email = "newEmp@email.com",
                position = "SAMPLE"
            ).toDto())
        }

        val expectedException = "409 CONFLICT \"Invalid Employee Position\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `modifyEmployeeStatus should return modified employee`() = runBlocking {
        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())

        val newEmployee = employeeEmployeeDatabaseHandler.modifyEmployeeStatus(employee.id!!)

        assertEquals(!employee.isActive, newEmployee.isActive )
    }

    @Test
    fun `modify employee status should throw and error given invalid employee id`() = runBlocking {
        val invalidId: Long = Constants.INVALID_ID
        employeeEmployeeRepository.save(EntityGenerator.createEmployee())

        val exception = assertFailsWith<ResponseStatusException> {
            employeeEmployeeDatabaseHandler.modifyEmployeeStatus(invalidId)
        }

        val expectedException = "404 NOT_FOUND \"Employee with id: $invalidId does not exist\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `get employee should return employee given valid id`() = runBlocking {
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeEmployeeRepository.save(employee)

        val employeeFromServiceImpl = employeeEmployeeDatabaseHandler.getById(createdEmployee.id!!)
        assertEquals(createdEmployee.id, employeeFromServiceImpl!!.id)
        assertEquals(createdEmployee.name, employeeFromServiceImpl.name)
        assertEquals(createdEmployee.email, employeeFromServiceImpl.email)
        assertEquals(createdEmployee.position, employeeFromServiceImpl.position)
    }

    @Test
    fun `update employee should return updated employee given valid inputs`() = runBlocking {
        val employee = EntityGenerator.createEmployee()
        val original = employeeEmployeeRepository.save(employee)

        val body = original.copy(
            name = "Juan Silva"
        )

        val updatedEmployee = employeeEmployeeDatabaseHandler.update(body.toDto(), original.id!!)
        assertEquals(body.name, updatedEmployee.name)
    }

    @Test
    fun `update employee should throw and error given invalid employee id`() = runBlocking {
        val invalidId: Long = Constants.INVALID_ID
        val employee = EntityGenerator.createEmployee()
        val original = employeeEmployeeRepository.save(employee)

        val body = original.copy(
            name = "Juan Silva"
        )

        val exception = assertFailsWith<ResponseStatusException> {
            employeeEmployeeDatabaseHandler.update(body.toDto(), invalidId)
        }

        val expectedException = "404 NOT_FOUND \"Employee with id: $invalidId does not exist\""

        assertEquals(expectedException, exception.message)
    }
}
