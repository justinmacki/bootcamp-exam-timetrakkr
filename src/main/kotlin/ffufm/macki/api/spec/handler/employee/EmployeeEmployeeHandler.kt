package ffufm.macki.api.spec.handler.employee

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.macki.api.spec.dbo.employee.EmployeeEmployeeDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface EmployeeEmployeeDatabaseHandler {
    /**
     * Create Employee: Creates a new Employee object
     * HTTP Code 201: The created Employee
     */
    suspend fun create(body: EmployeeEmployeeDTO): EmployeeEmployeeDTO

    /**
     * Finds Employees by ID: Returns Employees based on ID
     * HTTP Code 200: The Employee object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): EmployeeEmployeeDTO?

    /**
     * Set the status of the employee to Inactive/Active: Set the status of the employee to Inactive
     * or vice-versa.
     * HTTP Code 200: 
     */
    suspend fun modifyEmployeeStatus(id: Long): EmployeeEmployeeDTO

    /**
     * Update the Employee: Updates an existing Employee
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: EmployeeEmployeeDTO, id: Long): EmployeeEmployeeDTO
}

@Controller("employee.Employee")
class EmployeeEmployeeHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: EmployeeEmployeeDatabaseHandler

    /**
     * Create Employee: Creates a new Employee object
     * HTTP Code 201: The created Employee
     */
    @RequestMapping(value = ["/employees/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: EmployeeEmployeeDTO): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }

    /**
     * Finds Employees by ID: Returns Employees based on ID
     * HTTP Code 200: The Employee object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Set the status of the employee to Inactive/Active: Set the status of the employee to Inactive
     * or vice-versa.
     * HTTP Code 200: 
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/set-status/"], method = [RequestMethod.PUT])
    suspend fun modifyEmployeeStatus(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.modifyEmployeeStatus(id) }
    }

    /**
     * Update the Employee: Updates an existing Employee
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: EmployeeEmployeeDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
