package ffufm.macki.api.spec.handler.timerecord

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.macki.api.spec.dbo.timerecord.TimerecordTimeRecordDTO
import kotlin.Int
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface TimerecordTimeRecordDatabaseHandler {
    /**
     * Create TimeRecord: Creates a new TimeRecord object
     * HTTP Code 201: The created TimeRecord
     */
    suspend fun create(body: TimerecordTimeRecordDTO, id: Long): TimerecordTimeRecordDTO

    /**
     * Get all TimeRecords by page: Returns all TimeRecords from the system that the user has access
     * to. The Headers will include TotalElements, TotalPages, CurrentPage and PerPage to help with
     * Pagination.
     * HTTP Code 200: List of TimeRecords
     */
    suspend fun getAll(maxResults: Int = 100, page: Int = 0): Page<TimerecordTimeRecordDTO>

    /**
     * Finds TimeRecords by ID: Returns TimeRecords based on ID
     * HTTP Code 200: The TimeRecord object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): TimerecordTimeRecordDTO?

    /**
     * GetTotal Working Hours: 
     * HTTP Code 200: Returns the total working hours in a day
     */
    suspend fun getTotalWorkingHours(id: Long): Int

    /**
     * Update the TimeRecord: Updates an existing TimeRecord
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: TimerecordTimeRecordDTO, id: Long): TimerecordTimeRecordDTO
}

@Controller("timerecord.TimeRecord")
class TimerecordTimeRecordHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: TimerecordTimeRecordDatabaseHandler

    /**
     * Create TimeRecord: Creates a new TimeRecord object
     * HTTP Code 201: The created TimeRecord
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/timerecords/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: TimerecordTimeRecordDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body, id) }
    }

    /**
     * Get all TimeRecords by page: Returns all TimeRecords from the system that the user has access
     * to. The Headers will include TotalElements, TotalPages, CurrentPage and PerPage to help with
     * Pagination.
     * HTTP Code 200: List of TimeRecords
     */
    @RequestMapping(value = ["/timerecords/"], method = [RequestMethod.GET])
    suspend fun getAll(@RequestParam("maxResults") maxResults: Int? = 100, @RequestParam("page")
            page: Int? = 0): ResponseEntity<*> {

        return paging { databaseHandler.getAll(maxResults ?: 100, page ?: 0) }
    }

    /**
     * Finds TimeRecords by ID: Returns TimeRecords based on ID
     * HTTP Code 200: The TimeRecord object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/timerecords/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * GetTotal Working Hours: 
     * HTTP Code 200: Returns the total working hours in a day
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/time-records/total-working-hours/"], method =
            [RequestMethod.GET])
    suspend fun getTotalWorkingHours(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getTotalWorkingHours(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Update the TimeRecord: Updates an existing TimeRecord
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/timerecords/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: TimerecordTimeRecordDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
