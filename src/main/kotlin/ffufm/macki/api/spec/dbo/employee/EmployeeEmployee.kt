package ffufm.macki.api.spec.dbo.employee

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.macki.api.spec.dbo.employee.EmployeeAddressSerializer
import ffufm.macki.api.spec.dbo.employee.EmployeeContactDetailSerializer
import ffufm.macki.api.spec.dbo.employee.EmployeeEmployeeSerializer
import ffufm.macki.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import ffufm.macki.api.spec.dbo.leaverecord.LeaverecordLeaveRecordDTO
import ffufm.macki.api.spec.dbo.leaverecord.LeaverecordLeaveRecordSerializer
import ffufm.macki.api.spec.dbo.project.ProjectProjectSerializer
import ffufm.macki.api.spec.dbo.timerecord.TimerecordTimeRecord
import ffufm.macki.api.spec.dbo.timerecord.TimerecordTimeRecordDTO
import ffufm.macki.api.spec.dbo.timerecord.TimerecordTimeRecordSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Lob
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Boolean
import kotlin.Long
import kotlin.String
import kotlin.collections.List
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * Employee Entity
 */
@Entity(name = "EmployeeEmployee")
@Table(name = "employee_employee")
data class EmployeeEmployee(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Name of the employee
     * Sample: Greg Neu
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "name"
    )
    @Lob
    val name: String = "",
    /**
     * email of the employee
     * Sample: max.mustermann@bkl.de
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "email"
    )
    @Lob
    val email: String = "",
    /**
     * position of the employee
     * Sample: Monteur
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "position"
    )
    @Lob
    val position: String = "",
    /**
     * Whether the employee is active or not
     * Sample: True
     */
    @ColumnDefault("1")
    @Column(name = "is_active")
    val isActive: Boolean = true,
    @OneToOne(fetch = FetchType.LAZY)
    val superiorId: EmployeeEmployee? = null,
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val addresses: List<EmployeeAddress>? = mutableListOf(),
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val contactDetails: List<EmployeeContactDetail>? = mutableListOf(),
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val leaveRecords: List<LeaverecordLeaveRecord>? = mutableListOf(),
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val timeRecords: List<TimerecordTimeRecord>? = mutableListOf()
) : PassDTOModel<EmployeeEmployee, EmployeeEmployeeDTO, Long>() {
    override fun toDto(): EmployeeEmployeeDTO =
            super.toDtoInternal(EmployeeEmployeeSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<EmployeeEmployee, EmployeeEmployeeDTO, Long>,
            EmployeeEmployeeDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()

    enum class Userposition(
        val value: String
    ) {
        ABTEILUNGSLEITER("Abteilungsleiter"),

        MONTEUR("Monteur");
    }
}

/**
 * Employee Entity
 */
data class EmployeeEmployeeDTO(
    val id: Long? = null,
    /**
     * Name of the employee
     * Sample: Greg Neu
     */
    val name: String? = "",
    /**
     * email of the employee
     * Sample: max.mustermann@bkl.de
     */
    val email: String? = "",
    /**
     * position of the employee
     * Sample: Monteur
     */
    val position: String? = "",
    /**
     * Whether the employee is active or not
     * Sample: True
     */
    val isActive: Boolean? = true,
    val superiorId: EmployeeEmployeeDTO? = null,
    val addresses: List<EmployeeAddressDTO>? = null,
    val contactDetails: List<EmployeeContactDetailDTO>? = null,
    val leaveRecords: List<LeaverecordLeaveRecordDTO>? = null,
    val timeRecords: List<TimerecordTimeRecordDTO>? = null
) : PassDTO<EmployeeEmployee, Long>() {
    override fun toEntity(): EmployeeEmployee =
            super.toEntityInternal(EmployeeEmployeeSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<EmployeeEmployee, PassDTO<EmployeeEmployee, Long>,
            Long>, PassDTO<EmployeeEmployee, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class EmployeeEmployeeSerializer : PassDtoSerializer<EmployeeEmployee, EmployeeEmployeeDTO, Long>()
        {
    override fun toDto(entity: EmployeeEmployee): EmployeeEmployeeDTO = cycle(entity) {
        EmployeeEmployeeDTO(
                id = entity.id,
        name = entity.name,
        email = entity.email,
        position = entity.position,
        isActive = entity.isActive,
        superiorId = entity.superiorId?.idDto() ?: entity.superiorId?.toDto(),
        addresses = entity.addresses?.toSafeDtos(),
        contactDetails = entity.contactDetails?.toSafeDtos(),
        leaveRecords = entity.leaveRecords?.toSafeDtos(),
        timeRecords = entity.timeRecords?.toSafeDtos()
                )}

    override fun toEntity(dto: EmployeeEmployeeDTO): EmployeeEmployee = EmployeeEmployee(
            id = dto.id,
    name = dto.name ?: "",
    email = dto.email ?: "",
    position = dto.position ?: "",
    isActive = dto.isActive ?: true,
    superiorId = dto.superiorId?.toEntity(),
    addresses = dto.addresses?.toEntities() ?: emptyList(),
    contactDetails = dto.contactDetails?.toEntities() ?: emptyList(),
    leaveRecords = dto.leaveRecords?.toEntities() ?: emptyList(),
    timeRecords = dto.timeRecords?.toEntities() ?: emptyList()
            )
    override fun idDto(id: Long): EmployeeEmployeeDTO = EmployeeEmployeeDTO(
            id = id,
    name = null,
    email = null,
    position = null,
    isActive = null,

            )}

@Service("employee.EmployeeEmployeeValidator")
class EmployeeEmployeeValidator : PassModelValidation<EmployeeEmployee> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeEmployee>):
            ValidatorBuilder<EmployeeEmployee> = validatorBuilder.apply {
    }
}

@Service("employee.EmployeeEmployeeDTOValidator")
class EmployeeEmployeeDTOValidator : PassModelValidation<EmployeeEmployeeDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<EmployeeEmployeeDTO>):
            ValidatorBuilder<EmployeeEmployeeDTO> = validatorBuilder.apply {
    }
}
