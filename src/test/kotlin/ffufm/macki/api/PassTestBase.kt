package ffufm.macki.api

import com.fasterxml.jackson.databind.ObjectMapper
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.security.SpringSecurityAuditorAware
import ffufm.macki.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.macki.api.repositories.leaveRecord.LeaverecordLeaveRecordRepository
import ffufm.macki.api.repositories.project.ProjectProjectRepository
import ffufm.macki.api.repositories.timeRecord.TimerecordTimeRecordRepository
import ffufm.macki.api.spec.handler.employee.EmployeeEmployeeDatabaseHandler
import ffufm.macki.api.spec.handler.leaverecord.LeaverecordLeaveRecordDatabaseHandler
import ffufm.macki.api.spec.handler.timerecord.TimerecordTimeRecordDatabaseHandler
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc

@RunWith(SpringRunner::class)
@ActiveProfiles("test")
@SpringBootTest(classes = [SBMacki::class, SpringSecurityAuditorAware::class])
@AutoConfigureMockMvc
abstract class PassTestBase {
    @Autowired
    lateinit var context: ApplicationContext

    @Before
    fun initializeContext() {
        SpringContext.context = context
    }

    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    lateinit var employeeEmployeeDatabaseHandler: EmployeeEmployeeDatabaseHandler

    @Autowired
    lateinit var leaverecordLeaveRecordRepository: LeaverecordLeaveRecordRepository

    @Autowired
    lateinit var leaverecordLeaveRecordDatabaseHandler: LeaverecordLeaveRecordDatabaseHandler

    @Autowired
    lateinit var timerecordTimeRecordRepository: TimerecordTimeRecordRepository

    @Autowired
    lateinit var timerecordTimeRecordDatabaseHandler: TimerecordTimeRecordDatabaseHandler

    @Autowired
    lateinit var projectProjectRepository: ProjectProjectRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Before
    fun cleanRepositories() {
        employeeEmployeeRepository.deleteAll()
    }
}
