package ffufm.macki.api.spec.dbo.leaverecord

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.macki.api.spec.dbo.employee.EmployeeEmployee
import ffufm.macki.api.spec.dbo.employee.EmployeeEmployeeDTO
import ffufm.macki.api.spec.dbo.employee.EmployeeEmployeeSerializer
import java.time.LocalDate
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Boolean
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * Leave Record of the Employee
 */
@Entity(name = "LeaverecordLeaveRecord")
@Table(name = "leaverecord_leaverecord")
data class LeaverecordLeaveRecord(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Type of Leave. Can either be VL (Vacation Leave) or SL (Sick Leave)
     * Sample: VL
     */
    @Column(
        length = 2,
        updatable = true,
        nullable = false,
        name = "leave_type"
    )
    val leaveType: String = "",
    /**
     * Date of the proposed leave
     * Sample: 2021-12-21
     */
    @Column(name = "date")
    val date: LocalDate = LocalDate.now(),
    /**
     * Status of the leave whether approved or not.
     * Sample: true
     */
    @ColumnDefault("0")
    @Column(name = "is_approved")
    val isApproved: Boolean = false,
    /**
     * Comments/Remarks of the employee regarding the requested leave record
     * Sample: Will go to a family reunion
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "remarks"
    )
    @Lob
    val remarks: String = "",
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val employee: EmployeeEmployee? = null
) : PassDTOModel<LeaverecordLeaveRecord, LeaverecordLeaveRecordDTO, Long>() {
    override fun toDto(): LeaverecordLeaveRecordDTO =
            super.toDtoInternal(LeaverecordLeaveRecordSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<LeaverecordLeaveRecord, LeaverecordLeaveRecordDTO,
            Long>, LeaverecordLeaveRecordDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * Leave Record of the Employee
 */
data class LeaverecordLeaveRecordDTO(
    val id: Long? = null,
    /**
     * Type of Leave. Can either be VL (Vacation Leave) or SL (Sick Leave)
     * Sample: VL
     */
    val leaveType: String? = "",
    /**
     * Date of the proposed leave
     * Sample: 2021-12-21
     */
    val date: LocalDate? = LocalDate.now(),
    /**
     * Status of the leave whether approved or not.
     * Sample: true
     */
    val isApproved: Boolean? = false,
    /**
     * Comments/Remarks of the employee regarding the requested leave record
     * Sample: Will go to a family reunion
     */
    val remarks: String? = "",
    val employee: EmployeeEmployeeDTO? = null
) : PassDTO<LeaverecordLeaveRecord, Long>() {
    override fun toEntity(): LeaverecordLeaveRecord =
            super.toEntityInternal(LeaverecordLeaveRecordSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<LeaverecordLeaveRecord,
            PassDTO<LeaverecordLeaveRecord, Long>, Long>, PassDTO<LeaverecordLeaveRecord, Long>,
            Long>>)

    override fun readId(): Long? = this.id
}

@Component
class LeaverecordLeaveRecordSerializer : PassDtoSerializer<LeaverecordLeaveRecord,
        LeaverecordLeaveRecordDTO, Long>() {
    override fun toDto(entity: LeaverecordLeaveRecord): LeaverecordLeaveRecordDTO = cycle(entity) {
        LeaverecordLeaveRecordDTO(
                id = entity.id,
        leaveType = entity.leaveType,
        date = entity.date,
        isApproved = entity.isApproved,
        remarks = entity.remarks,
        employee = entity.employee?.idDto() ?: entity.employee?.toDto()
                )}

    override fun toEntity(dto: LeaverecordLeaveRecordDTO): LeaverecordLeaveRecord =
            LeaverecordLeaveRecord(
            id = dto.id,
    leaveType = dto.leaveType ?: "",
    date = dto.date ?: LocalDate.now(),
    isApproved = dto.isApproved ?: false,
    remarks = dto.remarks ?: "",
    employee = dto.employee?.toEntity()
            )
    override fun idDto(id: Long): LeaverecordLeaveRecordDTO = LeaverecordLeaveRecordDTO(
            id = id,
    leaveType = null,
    date = null,
    isApproved = null,
    remarks = null,

            )}

@Service("leaverecord.LeaverecordLeaveRecordValidator")
class LeaverecordLeaveRecordValidator : PassModelValidation<LeaverecordLeaveRecord> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<LeaverecordLeaveRecord>):
            ValidatorBuilder<LeaverecordLeaveRecord> = validatorBuilder.apply {
        konstraint(LeaverecordLeaveRecord::leaveType) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(2)
        }
        konstraintOnObject(LeaverecordLeaveRecord::employee) {
            notNull()
        }
    }
}

@Service("leaverecord.LeaverecordLeaveRecordDTOValidator")
class LeaverecordLeaveRecordDTOValidator : PassModelValidation<LeaverecordLeaveRecordDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<LeaverecordLeaveRecordDTO>):
            ValidatorBuilder<LeaverecordLeaveRecordDTO> = validatorBuilder.apply {
        konstraint(LeaverecordLeaveRecordDTO::leaveType) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(2)
        }
        konstraintOnObject(LeaverecordLeaveRecordDTO::employee) {
            notNull()
        }
    }
}
