package ffufm.macki.api.repositories.timeRecord

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.macki.api.spec.dbo.timerecord.TimerecordTimeRecord
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.sql.Date
import java.sql.Time
import java.time.LocalDate
import java.time.OffsetDateTime
import java.util.*

@Repository
interface TimerecordTimeRecordRepository : PassRepository<TimerecordTimeRecord, Long> {
    @Query(
        """
            SELECT CASE WHEN COUNT(tr) >= 2 THEN TRUE ELSE FALSE END
            FROM TimerecordTimeRecord tr WHERE tr.employee.id = :employeeId AND tr.date = :date
        """
    )
    fun checkTimeInsIfExceed(employeeId: Long, date: LocalDate?): Boolean

    @Query(
        """
            SELECT CASE WHEN COUNT(tr) > 0 THEN TRUE ELSE FALSE END
            FROM TimerecordTimeRecord tr WHERE :timeIn BETWEEN tr.timeIn AND tr.timeOut
            AND tr.date = :date AND tr.type = :type
        """
    )
    fun checkIfTimeInIsBetween(timeIn: Time?, date: LocalDate?, type: String): Boolean

    @Query(
        """
            SELECT CASE WHEN COUNT(tr) > 0 THEN TRUE ELSE FALSE END
            FROM TimerecordTimeRecord tr WHERE :timeOut BETWEEN tr.timeIn AND tr.timeOut
            AND tr.date = :date
        """
    )
    fun checkIfTimeOutIsBetween(timeOut: Time?, date: LocalDate?): Boolean

    @Query(
        """
            SELECT tr FROM TimerecordTimeRecord tr
            WHERE tr.date BETWEEN :fromDate AND :toDate
            AND tr.employee.id = :id
        """
    )
    fun getTimeRecordByEmployeeId(id: Long, fromDate: LocalDate?, toDate: LocalDate?): List<TimerecordTimeRecord>

    @Query(
        """
            SELECT tr.id, tr.timeIn, tr.timeOut FROM TimerecordTimeRecord tr
            WHERE tr.date = :date AND tr.employee.id = :id AND tr.type = :type
        """
    )
    fun getTotalHoursPerDay(id: Long, date: LocalDate?, type: String): List<String>
}