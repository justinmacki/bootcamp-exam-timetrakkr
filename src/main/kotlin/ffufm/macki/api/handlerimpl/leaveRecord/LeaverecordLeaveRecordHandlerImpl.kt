package ffufm.macki.api.handlerimpl.leaveRecord

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.macki.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.macki.api.repositories.leaveRecord.LeaverecordLeaveRecordRepository
import ffufm.macki.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import ffufm.macki.api.spec.dbo.leaverecord.LeaverecordLeaveRecordDTO
import ffufm.macki.api.spec.handler.leaverecord.LeaverecordLeaveRecordDatabaseHandler
import ffufm.macki.api.utils.Constants
import ffufm.macki.api.utils.EmployeePositionEnum
import ffufm.macki.api.utils.LeaveTypeEnum
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException
import java.time.LocalDate
import java.time.temporal.ChronoUnit

@Component("leaverecord.LeaverecordLeaveRecordHandler")
class LeaverecordLeaveRecordHandlerImpl (
    private val employeeRepository: EmployeeEmployeeRepository
        ) : PassDatabaseHandler<LeaverecordLeaveRecord,
        LeaverecordLeaveRecordRepository>(), LeaverecordLeaveRecordDatabaseHandler{

    override suspend fun approveLeave(employeeId: Long, id: Long): LeaverecordLeaveRecordDTO {
        val employee = employeeRepository.findById(employeeId).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $employeeId not found")
        }

        val leaveRecord = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Leave Record with id: $id not found")
        }

        if(employee.position != EmployeePositionEnum.ABTEILUNGSLEITER.value){
            throw ResponseStatusException(HttpStatus.UNAUTHORIZED,
                "You do not have a permission to approve leave.")
        }

        if(employee.id == leaveRecord.employee!!.id) {
            throw ResponseStatusException(HttpStatus.CONFLICT,
                "You cannot approve your own leave.")
        }

        return repository.save(leaveRecord.copy( isApproved = true )).toDto()
    }

    override suspend fun create(body: LeaverecordLeaveRecordDTO, id: Long): LeaverecordLeaveRecordDTO {
        val employee = employeeRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id not found")
        }

        verifyLeaveDetails(body, id)

        return repository.save(body.copy( employee = employee.toDto() ).toEntity()).toDto()
    }

    override suspend fun getAll(id: Long, maxResults: Int, page: Int): Page<LeaverecordLeaveRecordDTO> {
        TODO("Not yet implemented")
    }

    override suspend fun getById(id: Long): LeaverecordLeaveRecordDTO? {
        TODO("Not yet implemented")
    }

    override suspend fun getNoOfRemainingVL(id: Long): Int {
        employeeRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id not found")
        }

        return (Constants.MAX_VL_DAYS_PER_YEAR - repository.getRemainingLeave(id, LocalDate.now(),
            LeaveTypeEnum.VL.value))
    }

    override suspend fun getTotalAnnualLeaves(id: Long): Int {
        employeeRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id not found")
        }

        return repository.getTotalLeavesInCurrentYear(id, LocalDate.now(), LeaveTypeEnum.VL.value)
    }

    override suspend fun getTotalLeaveRequests(id: Long): Int {
        employeeRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id not found")
        }

        return repository.getLeaveRequests(id, LeaveTypeEnum.VL.value)
    }

    override suspend fun getTotalSickDays(id: Long): Int {
        return repository.getTotalLeavesInCurrentYear(id, LocalDate.now(), LeaveTypeEnum.SL.value)
    }

    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.delete(original)
    }

    override suspend fun update(body: LeaverecordLeaveRecordDTO, id: Long): LeaverecordLeaveRecordDTO {
        TODO("Not yet implemented")
    }

    fun verifyLeaveDetails(body: LeaverecordLeaveRecordDTO, employeeId: Long){
        try {
            val leaveType = LeaveTypeEnum.valueOf(requireNotNull(body.leaveType).uppercase())

            when (leaveType.toString()) {
                LeaveTypeEnum.VL.value -> {
                    val days = LocalDate.now().until(body.date, ChronoUnit.DAYS)

                    if (days <= Constants.MAX_VL_LEAVE_DAYS) {
                        throw ResponseStatusException(
                            HttpStatus.BAD_REQUEST,
                            "You can only file for a leave 14 days in advance."
                        )
                    }

                    if (repository.checkIfAvailableVL(employeeId, body.date,
                            LeaveTypeEnum.VL.value)) {
                        throw ResponseStatusException(
                            HttpStatus.BAD_REQUEST,
                            "Employee does not have enough Vacation Leave this month"
                        )
                    }
                }
                LeaveTypeEnum.SL.value -> {
                    if (repository.checkIfAvailableSL(employeeId, body.date,
                            LeaveTypeEnum.SL.value)) {
                        throw ResponseStatusException(
                            HttpStatus.BAD_REQUEST,
                            "Employee does not have enough Sick Leave this month"
                        )
                    }
                }
            }
        }catch(e: IllegalArgumentException){
            throw ResponseStatusException(HttpStatus.CONFLICT, "Invalid Leave Type")
        }
    }
}