package ffufm.macki.api.spec.handler.timeRecord.implementation

import ffufm.macki.api.PassTestBase
import ffufm.macki.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.web.server.ResponseStatusException
import java.sql.Time
import java.time.LocalDate
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class TimerecordTimeRecordDatabaseHandlerTest : PassTestBase() {

    @Test
    fun `test getTotalWorkingHours`() = runBlocking {
        val employee = EntityGenerator.createEmployee().toDto()
        val createdEmployee = employeeEmployeeDatabaseHandler.create(employee)

        val createdProject = projectProjectRepository.save(EntityGenerator.createProject().copy(
            owner = createdEmployee.toEntity()
        ))

        val createdTimeRecord = timerecordTimeRecordRepository.save(EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee.toEntity(),
            project = createdProject
        ))

        val secondCreatedTimeRecord = timerecordTimeRecordRepository.save(EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee.toEntity(),
            project = createdProject,
            timeIn = Time.valueOf("13:00:00"),
            timeOut = Time.valueOf("18:00:00"),
        ))

        val count = timerecordTimeRecordDatabaseHandler.getTotalWorkingHours(createdEmployee.id!!)

        assertEquals(8, count)
    }

    @Test
    fun `create should error given third timeIn`() = runBlocking {
        val employee = EntityGenerator.createEmployee().toDto()
        val createdEmployee = employeeEmployeeDatabaseHandler.create(employee)

        val createdProject = projectProjectRepository.save(EntityGenerator.createProject().copy(
            owner = createdEmployee.toEntity()
        ))

        val createdTimeRecord = timerecordTimeRecordRepository.save(EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee.toEntity(),
            project = createdProject
        ))

        val secondCreatedTimeRecord = timerecordTimeRecordRepository.save(EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee.toEntity(),
            project = createdProject,
            timeIn = Time.valueOf("13:00:00"),
            timeOut = Time.valueOf("18:00:00")
        ))

        val body = secondCreatedTimeRecord.copy(
            timeIn = Time.valueOf("19:00:00"),
            timeOut = Time.valueOf("23:00:00")
        )

        val exception = assertFailsWith<ResponseStatusException> {
            timerecordTimeRecordDatabaseHandler.create(body.toDto(), createdEmployee.id!!)
        }

        val expectedException = "400 BAD_REQUEST \"You cannot create more than two timeIns per day.\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create timeRecord should return timeRecord`() = runBlocking {
        assertEquals(0, timerecordTimeRecordRepository.findAll().count())

        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeEmployeeDatabaseHandler.create(employee.toDto())

        val createdProject = projectProjectRepository.save(EntityGenerator.createProject().copy(
            owner = createdEmployee.toEntity()
        ))

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee.toEntity(),
            project = createdProject
        )

        val createdTimeRecord = timerecordTimeRecordDatabaseHandler.create(timeRecord.toDto(), createdEmployee.id!!)

        assertEquals(timeRecord.timeIn, createdTimeRecord.timeIn)
        assertEquals(timeRecord.timeOut, createdTimeRecord.timeOut)
        assertEquals(timeRecord.date, createdTimeRecord.date)
    }

    @Test
    fun `create timeRecord should fail given invalid employee id`() = runBlocking {
        val invalidId: Long = 423456

        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeEmployeeDatabaseHandler.create(employee.toDto())

        val createdProject = projectProjectRepository.save(EntityGenerator.createProject().copy(
            owner = createdEmployee.toEntity()
        ))

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee.toEntity(),
            project = createdProject
        )

        val exception = assertFailsWith<ResponseStatusException> {
            timerecordTimeRecordDatabaseHandler.create(timeRecord.toDto(), invalidId)
        }

        val expectedException = "404 NOT_FOUND \"User with id: $invalidId not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create timeRecord should fail given timeOut is before timeIn`() = runBlocking {
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeEmployeeDatabaseHandler.create(employee.toDto())

        val createdProject = projectProjectRepository.save(EntityGenerator.createProject().copy(
            owner = createdEmployee.toEntity()
        ))

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee.toEntity(),
            project = createdProject,
            timeIn = Time.valueOf("11:00:00"),
            timeOut = Time.valueOf("08:00:00")
        )

        val exception = assertFailsWith<ResponseStatusException> {
            timerecordTimeRecordDatabaseHandler.create(timeRecord.toDto(), createdEmployee.id!!)
        }

        val expectedException = "400 BAD_REQUEST \"Time In should be before Time Out!\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create timeRecord should fail given timeRecord exceeds maximum hours per record`() = runBlocking {
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeEmployeeDatabaseHandler.create(employee.toDto())

        val createdProject = projectProjectRepository.save(EntityGenerator.createProject().copy(
            owner = createdEmployee.toEntity()
        ))

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee.toEntity(),
            project = createdProject,
            timeIn = Time.valueOf("01:00:00"),
            timeOut = Time.valueOf("12:00:00")
        )

        val exception = assertFailsWith<ResponseStatusException> {
            timerecordTimeRecordDatabaseHandler.create(timeRecord.toDto(), createdEmployee.id!!)
        }

        val expectedException = "400 BAD_REQUEST \"You exceed the maximum hours per record!\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create timeRecord should fail given overlapping timeIns with same type`() = runBlocking {
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeEmployeeDatabaseHandler.create(employee.toDto())

        val createdProject = projectProjectRepository.save(EntityGenerator.createProject().copy(
            owner = createdEmployee.toEntity()
        ))

        timerecordTimeRecordRepository.save(EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee.toEntity(),
            project = createdProject
        ))

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee.toEntity(),
            project = createdProject,
            timeIn = Time.valueOf("11:00:00"),
            timeOut = Time.valueOf("15:00:00")
        )

        val exception = assertFailsWith<ResponseStatusException> {
            timerecordTimeRecordDatabaseHandler.create(timeRecord.toDto(), createdEmployee.id!!)
        }

        val expectedException = "400 BAD_REQUEST \"TimeIns cannot overlap!\""

        assertEquals(expectedException, exception.message)
    }
}