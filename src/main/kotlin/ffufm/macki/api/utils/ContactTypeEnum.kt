package ffufm.macki.api.utils

enum class ContactTypeEnum(val value: String) {
    CONTACT_TYPE_MOBILE("Mobile Phone"),
    CONTACT_TYPE_TEL("Telephone"),
    CONTACT_TYPE_FAM("Family Phone"),
    CONTACT_TYPE_EMAIL("Email")
}