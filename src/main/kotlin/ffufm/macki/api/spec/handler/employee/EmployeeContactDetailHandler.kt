package ffufm.macki.api.spec.handler.employee

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.macki.api.spec.dbo.employee.EmployeeContactDetailDTO
import kotlin.Int
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface EmployeeContactDetailDatabaseHandler {
    /**
     * Create ContactDetail: Creates a new ContactDetail object
     * HTTP Code 201: The created ContactDetail
     */
    suspend fun create(body: EmployeeContactDetailDTO, id: Long): EmployeeContactDetailDTO

    /**
     * Find by Employee: Finds ContactDetails by the parent Employee id
     * HTTP Code 200: List of ContactDetails items
     */
    suspend fun getByEmployee(
        id: Long,
        maxResults: Int = 100,
        page: Int = 0
    ): Page<EmployeeContactDetailDTO>

    /**
     * Finds ContactDetails by ID: Returns ContactDetails based on ID
     * HTTP Code 200: The ContactDetail object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): EmployeeContactDetailDTO?

    /**
     * Delete ContactDetail by id.: Deletes one specific ContactDetail.
     */
    suspend fun remove(id: Long)

    /**
     * Update the ContactDetail: Updates an existing ContactDetail
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: EmployeeContactDetailDTO, id: Long): EmployeeContactDetailDTO
}

@Controller("employee.ContactDetail")
class EmployeeContactDetailHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: EmployeeContactDetailDatabaseHandler

    /**
     * Create ContactDetail: Creates a new ContactDetail object
     * HTTP Code 201: The created ContactDetail
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/contact-details/"], method =
            [RequestMethod.POST])
    suspend fun create(@RequestBody body: EmployeeContactDetailDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body, id) }
    }

    /**
     * Find by Employee: Finds ContactDetails by the parent Employee id
     * HTTP Code 200: List of ContactDetails items
     */
    @RequestMapping(value = ["/employees/{id:\\d+}/contact-details/"], method = [RequestMethod.GET])
    suspend fun getByEmployee(
        @PathVariable("id") id: Long,
        @RequestParam("maxResults") maxResults: Int? = 100,
        @RequestParam("page") page: Int? = 0
    ): ResponseEntity<*> {

        return paging { databaseHandler.getByEmployee(id, maxResults ?: 100, page ?: 0) }
    }

    /**
     * Finds ContactDetails by ID: Returns ContactDetails based on ID
     * HTTP Code 200: The ContactDetail object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/contact-details/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Delete ContactDetail by id.: Deletes one specific ContactDetail.
     */
    @RequestMapping(value = ["/contact-details/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the ContactDetail: Updates an existing ContactDetail
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/contact-details/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: EmployeeContactDetailDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
