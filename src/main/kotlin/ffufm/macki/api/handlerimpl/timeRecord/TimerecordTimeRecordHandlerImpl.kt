package ffufm.macki.api.handlerimpl.timeRecord

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import ffufm.macki.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.macki.api.repositories.timeRecord.TimerecordTimeRecordRepository
import ffufm.macki.api.spec.dbo.timerecord.TimerecordTimeRecord
import ffufm.macki.api.spec.dbo.timerecord.TimerecordTimeRecordDTO
import ffufm.macki.api.spec.handler.timerecord.TimerecordTimeRecordDatabaseHandler
import ffufm.macki.api.utils.Constants
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException
import java.time.Duration
import java.time.LocalDate
import java.time.LocalTime

@Component("timerecord.TimerecordTimeRecordHandler")
class TimerecordTimeRecordHandlerImpl (
    private val employeeRepository: EmployeeEmployeeRepository
        ) : PassDatabaseHandler<TimerecordTimeRecord,
        TimerecordTimeRecordRepository>(), TimerecordTimeRecordDatabaseHandler {

    override suspend fun create(body: TimerecordTimeRecordDTO, id: Long): TimerecordTimeRecordDTO {
        val employee = employeeRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id not found")
        }

        checkTimeDetails(body, id)

        return repository.save(body.copy( employee = employee.toDto() ).toEntity()).toDto()
    }

    override suspend fun getAll(maxResults: Int, page: Int): Page<TimerecordTimeRecordDTO> {
        TODO("Not yet implemented")
    }

    override suspend fun getById(id: Long): TimerecordTimeRecordDTO? {
        TODO("Not yet implemented")
    }

    override suspend fun getTotalWorkingHours(id: Long): Int {
        val data = repository.getTotalHoursPerDay(id, LocalDate.now(), "Arbeitszeit")
        var hours: Long = 0
        for (e in data){
            val store = e.split(",")
            val numberOfHours = Duration.between(LocalTime.parse(store[1]), LocalTime.parse(store[2])).toHours()
            hours += numberOfHours
        }
        return hours.toInt()
    }

    override suspend fun update(body: TimerecordTimeRecordDTO, id: Long): TimerecordTimeRecordDTO {
        val timeRecord = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Time Record with id: $id does not exist")
        }

        checkTimeDetails(body, id)

        return repository.save(timeRecord.copy(
            timeIn = requireNotNull(body.timeIn),
            timeOut = requireNotNull(body.timeOut),
            date = requireNotNull(body.date),
            comment = requireNotNull(body.comment)
        )).toDto()
    }

    fun checkTimeDetails(body: TimerecordTimeRecordDTO, employeeId: Long){
        if(body.timeIn == body.timeOut){
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Time In and Time Out cannot be the same.")
        }

        if(requireNotNull(body.timeOut).before(body.timeIn)){
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Time In should be before Time Out!")
        }

        val numberOfHours = Duration.between(requireNotNull(body.timeIn).toLocalTime(),
            requireNotNull(body.timeOut).toLocalTime()).toHours()

        if(numberOfHours > Constants.MAX_HOURS_PER_RECORD){
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "You exceed the maximum hours per record!")
        }

        if(repository.checkTimeInsIfExceed(employeeId, body.date)){
            throw ResponseStatusException (
                HttpStatus.BAD_REQUEST,
                "You cannot create more than two timeIns per day.")
        }

        if(repository.checkIfTimeInIsBetween(body.timeIn, body.date, requireNotNull(body.type))){
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TimeIns cannot overlap!")
        }

        if(repository.checkIfTimeOutIsBetween(body.timeOut, body.date)){
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TimeOuts cannot overlap!")
        }
    }
}