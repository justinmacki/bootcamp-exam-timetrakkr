package ffufm.macki.api.repositories.employee

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.macki.api.spec.dbo.employee.EmployeeContactDetail
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface EmployeeContactDetailRepository : PassRepository<EmployeeContactDetail, Long> {
    @Query(
        """
                SELECT CASE WHEN COUNT(cd) > 0 THEN TRUE ELSE FALSE END
                FROM EmployeeContactDetail cd WHERE cd.contactDetails = :contactDetails
            """
    )
    fun doesContactDetailExist(contactDetails: String): Boolean

    @Query(
        """
            SELECT COUNT(cd) FROM EmployeeContactDetail cd
            WHERE cd.employee.id = :id
        """
    )
    fun countContactDetailsByUserId(id: Long): Int

    @Modifying
    @Transactional
    @Query(
        """
            UPDATE EmployeeContactDetail c SET c.isPrimary = false
            WHERE c.employee.id = :userId
        """
    )
    fun updateIsPrimaryToFalse(userId: Long): Unit
}