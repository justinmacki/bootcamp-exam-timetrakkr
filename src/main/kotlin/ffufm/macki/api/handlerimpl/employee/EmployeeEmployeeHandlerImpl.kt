package ffufm.macki.api.handlerimpl.employee

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.macki.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.macki.api.spec.dbo.employee.EmployeeEmployee
import ffufm.macki.api.spec.dbo.employee.EmployeeEmployeeDTO
import ffufm.macki.api.spec.handler.employee.EmployeeEmployeeDatabaseHandler
import ffufm.macki.api.utils.EmployeePositionEnum
import kotlin.Long
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("employee.EmployeeEmployeeHandler")
class EmployeeEmployeeHandlerImpl : PassDatabaseHandler<EmployeeEmployee,
        EmployeeEmployeeRepository>(), EmployeeEmployeeDatabaseHandler {
    /**
     * Create Employee: Creates a new Employee object
     * HTTP Code 201: The created Employee
     */
    override suspend fun create(body: EmployeeEmployeeDTO): EmployeeEmployeeDTO {
        val bodyEntity = body.toEntity()

        if(repository.doesEmailExist(bodyEntity.email)){
            throw ResponseStatusException(HttpStatus.CONFLICT, "Email ${bodyEntity.email} already exists")
        }
        try{
            val userType = EmployeePositionEnum.valueOf(bodyEntity.position.uppercase())

            return repository.save(bodyEntity.copy(position = userType.value)).toDto()
        }catch (e: Exception){
            throw ResponseStatusException(HttpStatus.CONFLICT, "Invalid Employee Position")
        }

    }

    /**
     * Finds Employees by ID: Returns Employees based on ID
     * HTTP Code 200: The Employee object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): EmployeeEmployeeDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    override suspend fun modifyEmployeeStatus(id: Long): EmployeeEmployeeDTO {
        val employee = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id does not exist")
        }

        val modifyIsActive = !employee.isActive

        return repository.save(employee.copy( isActive = modifyIsActive )).toDto()
    }

    /**
     * Update the Employee: Updates an existing Employee
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: EmployeeEmployeeDTO, id: Long): EmployeeEmployeeDTO {
        val employee = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id does not exist")
        }
        val bodyEntity = body.toEntity()

        return repository.save(employee.copy(
            name = bodyEntity.name,
            email = bodyEntity.email,
            position = bodyEntity.position
        )).toDto()
    }
}
