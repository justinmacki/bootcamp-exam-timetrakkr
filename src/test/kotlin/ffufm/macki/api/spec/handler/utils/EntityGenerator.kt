package ffufm.macki.api.spec.handler.utils

import ffufm.macki.api.spec.dbo.employee.EmployeeEmployee
import ffufm.macki.api.spec.dbo.leaverecord.LeaverecordLeaveRecord
import ffufm.macki.api.spec.dbo.project.ProjectProject
import ffufm.macki.api.spec.dbo.timerecord.TimerecordTimeRecord
import ffufm.macki.api.utils.EmployeePositionEnum
import ffufm.macki.api.utils.LeaveTypeEnum
import java.sql.Date
import java.sql.Time
import java.time.LocalDate
import java.time.OffsetDateTime

object EntityGenerator {

    fun createEmployee(): EmployeeEmployee = EmployeeEmployee(
        name = "Brandon Cruz",
        email = "brandon@brandon.com",
        position = EmployeePositionEnum.MONTEUR.value
    )

    fun createLeaveRecord(): LeaverecordLeaveRecord = LeaverecordLeaveRecord(
        leaveType = LeaveTypeEnum.VL.value,
        date = LocalDate.parse("2022-02-23"),
        remarks = "Will go to a family reunion"
    )

    fun createTimeRecord(): TimerecordTimeRecord = TimerecordTimeRecord(
        timeIn = Time.valueOf("09:00:00"),
        timeOut = Time.valueOf("12:00:00"),
        date = LocalDate.now(),
        type = "Arbeitszeit",
        comment = "Had an OT due to some reason."
    )

    fun createProject(): ProjectProject = ProjectProject(
        category = "Site Preparation",
        name = "Project A"
    )
}