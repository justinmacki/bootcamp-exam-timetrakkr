package ffufm.macki.api.utils

enum class EmployeePositionEnum( val value: String) {
    MONTEUR("MONTEUR"),
    ABTEILUNGSLEITER("ABTEILUNGSLEITER")
}