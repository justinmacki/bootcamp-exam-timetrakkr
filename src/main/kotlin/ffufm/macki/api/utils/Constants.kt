package ffufm.macki.api.utils

object Constants {
    const val INVALID_ID: Long = 0

    const val MAX_CONTACT_DETAILS = 4
    const val MAX_HOURS_PER_RECORD = 10
    const val MAX_VL_LEAVE_DAYS = 14
    const val MAX_VL_DAYS_PER_MONTH = 5
    const val MAX_VL_DAYS_PER_YEAR = MAX_VL_DAYS_PER_MONTH * 12
    const val MAX_SL_DAYS_PER_MONTH = 2
}