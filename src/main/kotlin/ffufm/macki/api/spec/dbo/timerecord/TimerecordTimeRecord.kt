package ffufm.macki.api.spec.dbo.timerecord

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import ffufm.macki.api.spec.dbo.employee.EmployeeEmployee
import ffufm.macki.api.spec.dbo.employee.EmployeeEmployeeDTO
import ffufm.macki.api.spec.dbo.project.ProjectProject
import ffufm.macki.api.spec.dbo.project.ProjectProjectDTO
import java.time.LocalDate
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.Table
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import java.sql.Date
import java.sql.Time

/**
 * Time Record of the employee
 */
@Entity(name = "TimerecordTimeRecord")
@Table(name = "timerecord_timerecord")
data class TimerecordTimeRecord(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Time in of the employee
     * Sample: 09:00:00
     */
    @Column(name = "time_in")
    val timeIn: Time,
    /**
     * Time Out of the Employee
     * Sample: 18:00:00
     */
    @Column(name = "time_out")
    val timeOut: Time,
    /**
     * Date of the time record
     * Sample: 2022-01-19
     */
    @Column(name = "date")
    val date: LocalDate = LocalDate.now(),
    /**
     * Type of the time record, whether Pause, Wartezeit, Bereitschaftszeit, or Arbeitszeit
     * Sample: Pause
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "type"
    )
    @Lob
    val type: String = "",
    /**
     * Comment/remarks regarding the time record
     * Sample: Had an OT due to some reason.
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "comment"
    )
    @Lob
    val comment: String = "",
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    val employee: EmployeeEmployee? = null,
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val project: ProjectProject? = null
) : PassDTOModel<TimerecordTimeRecord, TimerecordTimeRecordDTO, Long>() {
    override fun toDto(): TimerecordTimeRecordDTO =
            super.toDtoInternal(TimerecordTimeRecordSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<TimerecordTimeRecord, TimerecordTimeRecordDTO,
            Long>, TimerecordTimeRecordDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * Time Record of the employee
 */
data class TimerecordTimeRecordDTO(
    val id: Long? = null,
    /**
     * Time in of the employee
     * Sample: 09:00:00
     */
    val timeIn: Time?,
    /**
     * Time Out of the Employee
     * Sample: 18:00:00
     */
    val timeOut: Time?,
    /**
     * Date of the time record
     * Sample: 2022-01-19
     */
    val date: LocalDate? = LocalDate.now(),
    /**
     * Type of the time record, whether Pause, Wartezeit, Bereitschaftszeit, or Arbeitszeit
     * Sample: Pause
     */
    val type: String? = "",
    /**
     * Comment/remarks regarding the time record
     * Sample: Had an OT due to some reason.
     */
    val comment: String? = "",
    val employee: EmployeeEmployeeDTO? = null,
    val project: ProjectProjectDTO? = null
) : PassDTO<TimerecordTimeRecord, Long>() {
    override fun toEntity(): TimerecordTimeRecord =
            super.toEntityInternal(TimerecordTimeRecordSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<TimerecordTimeRecord,
            PassDTO<TimerecordTimeRecord, Long>, Long>, PassDTO<TimerecordTimeRecord, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class TimerecordTimeRecordSerializer : PassDtoSerializer<TimerecordTimeRecord,
        TimerecordTimeRecordDTO, Long>() {
    override fun toDto(entity: TimerecordTimeRecord): TimerecordTimeRecordDTO = cycle(entity) {
        TimerecordTimeRecordDTO(
                id = entity.id,
        timeIn = entity.timeIn,
        timeOut = entity.timeOut,
        date = entity.date,
        type = entity.type,
        comment = entity.comment,
        employee = entity.employee?.idDto() ?: entity.employee?.toDto(),
        project = entity.project?.idDto() ?: entity.project?.toDto()
                )}

    override fun toEntity(dto: TimerecordTimeRecordDTO): TimerecordTimeRecord =
            TimerecordTimeRecord(
            id = dto.id,
    timeIn = requireNotNull(dto.timeIn),
    timeOut = requireNotNull(dto.timeOut),
    date = dto.date ?: LocalDate.now(),
    type = dto.type ?: "",
    comment = dto.comment ?: "",
    employee = dto.employee?.toEntity(),
    project = dto.project?.toEntity()
            )
    override fun idDto(id: Long): TimerecordTimeRecordDTO = TimerecordTimeRecordDTO(
            id = id,
    timeIn = null,
    timeOut = null,
    date = null,
    type = null,
    comment = null,

            )}

@Service("timerecord.TimerecordTimeRecordValidator")
class TimerecordTimeRecordValidator : PassModelValidation<TimerecordTimeRecord> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<TimerecordTimeRecord>):
            ValidatorBuilder<TimerecordTimeRecord> = validatorBuilder.apply {
        konstraintOnObject(TimerecordTimeRecord::project) {
            notNull()
        }
    }
}

@Service("timerecord.TimerecordTimeRecordDTOValidator")
class TimerecordTimeRecordDTOValidator : PassModelValidation<TimerecordTimeRecordDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<TimerecordTimeRecordDTO>):
            ValidatorBuilder<TimerecordTimeRecordDTO> = validatorBuilder.apply {
        konstraintOnObject(TimerecordTimeRecordDTO::project) {
            notNull()
        }
    }
}
