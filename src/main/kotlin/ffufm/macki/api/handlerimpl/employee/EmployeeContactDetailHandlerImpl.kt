package ffufm.macki.api.handlerimpl.employee

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.*
import ffufm.macki.api.repositories.employee.EmployeeContactDetailRepository
import ffufm.macki.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.macki.api.spec.dbo.employee.EmployeeContactDetail
import ffufm.macki.api.spec.dbo.employee.EmployeeContactDetailDTO
import ffufm.macki.api.spec.handler.employee.EmployeeContactDetailDatabaseHandler
import ffufm.macki.api.utils.Constants
import ffufm.macki.api.utils.EmployeePositionEnum
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("employee.EmployeeContactDetailHandler")
class EmployeeContactDetailHandlerImpl(
    private val employeeRepository : EmployeeEmployeeRepository
) : PassDatabaseHandler<EmployeeContactDetail,
        EmployeeContactDetailRepository>(), EmployeeContactDetailDatabaseHandler {

    override suspend fun create(body: EmployeeContactDetailDTO, id: Long): EmployeeContactDetailDTO {
        val bodyEntity = body.toEntity()
        val employee = employeeRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id not found")
        }

        if(repository.doesContactDetailExist(bodyEntity.contactDetails)){
            throw ResponseStatusException(HttpStatus.CONFLICT, "Contact Details ${bodyEntity.contactDetails} already exists")
        }

        if(repository.countContactDetailsByUserId(id) >= Constants.MAX_CONTACT_DETAILS){
            throw ResponseStatusException (HttpStatus.BAD_REQUEST, "You cannot create more than four details.")
        }

        try{
            val contactType = EmployeePositionEnum.valueOf(bodyEntity.contactType.uppercase())

            if(bodyEntity.isPrimary){
                // Create a query that will update the value of all contactDetails isPrimary=false
                repository.updateIsPrimaryToFalse(id)
            }

            return repository.save(bodyEntity.copy(
                employee = employee,
                contactType = contactType.value
            )).toDto()
        }catch (e: Exception){
            throw ResponseStatusException(HttpStatus.CONFLICT, "Invalid Contact Type")
        }
    }

    override suspend fun getByEmployee(id: Long, maxResults: Int, page: Int): Page<EmployeeContactDetailDTO> {
        val pagination = PageRequest.of(page ?: 0, maxResults ?: 100)
        return repository.findAll(pagination).toDtos()
    }

    override suspend fun getById(id: Long): EmployeeContactDetailDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.delete(original)    }

    override suspend fun update(body: EmployeeContactDetailDTO, id: Long): EmployeeContactDetailDTO {
        val bodyEntity = body.toEntity()
        val original = repository.findById(id).orElseThrow404(id)

        return repository.save(original.copy(
            contactDetails = bodyEntity.contactDetails,
            contactType = bodyEntity.contactType
        )).toDto()
    }
}